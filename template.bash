#!/bin/bash

#BSUB -L /bin/bash
##BSUB -u iakov.davydov@unil.ch
##BSUB -N
#BSUB -o output-%J-%I.txt
#BSUB -e error-%J-%I.txt
#BSUB -J JNAME[1-100]
#BSUB –R "select[tmp>1024],rusage[mem=4096]"
#BSUB -M 4194304

NAME=JNAME
SOURCE=JNAME.tar
source $HOME/mysub/mysub.bash

cmd () {
	echo $1 > res/${1%.*}.res
	# imitate a long job
	sleep 60
}

run
