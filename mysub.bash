PORT=$(( 9888+($LSB_JOBID%100) ))
MONTHLY=${MONTHLY:-/scratch/beegfs/monthly}
CLUSTER=$MONTHLY/$USER/$NAME
LONG_INDEX=`printf "%03d" $LSB_JOBINDEX`
LOCAL=/scratch/local/weekly/$USER-$NAME.$LSB_JOBID.$LONG_INDEX
HOSTF=$CLUSTER/$LSB_JOBID.host

run () {
	if [[ -z $NAME || -z $SOURCE ]]
	then
		echo NAME or SOURCE are not set.
		exit 1
	fi


	# If source is relative path convert it to full.
	if [[ $SOURCE != /* ]]
	then
		SRC=$CLUSTER/$SOURCE
	else
		SRC=$SOURCE
	fi

	mkdir -p $LOCAL/res
	cd $LOCAL
	if [[ $LSB_JOBINDEX == 1 ]]
	then
		hostname > $HOSTF
		server
	fi
	tar axmf $SRC
	trap handler USR2 INT
	process
	cleanup
}

server () {
	case $SRC in
		*.txt)
			cat $SRC | lsrv.py $PORT &
			;;
		*) 
			tar atf $SRC | lsrv.py $PORT &
			;;
	esac
	SPID=$!
}

process () {
	i=0
	while [[ -z $HOST && $i < 20 ]]
	do
		sleep 5
		HOST=`cat $HOSTF`
		((i++))
	done

	if [[ -z $HOST ]]
	then
		echo "Couldn't find host file $HOSTF"
		return
	fi

	i=0
	while true
	do
		n=`echo $LSB_JOBID | nc $HOST $PORT`
		if [[ $n == END ]]
		then
			break
		fi
		if [[ -z $n ]]
		then
			sleep 5
			((i++))
			if [[ $i > 20 ]]
			then
				echo Couldnt read from $HOST
				return
			fi
		else
			# If it is a directory do nothing.
			if [[ -d $n ]]
			then
				continue
			fi
			dn=`dirname $n`
			mkdir -p res/$dn
			cmd $n > res/$n.out 2> res/$n.err &
			CPID=$!
			wait $CPID
			CPID=""
		fi
	done
}

handler () {
	if [[ -n $CPID ]]
	then
		kill -s USR2 $CPID
		wait $CPID
	fi
	if [[ -n $SPID ]]
	then
		kill $SPID
	fi
	cleanup
	echo "Exit by signal"
	exit 2
}

cleanup () {
	cd res
	find -size 0 \( -name "*.out" -o -name "*.err" \) -delete
	tar zcf $CLUSTER/res-$SUB-$LSB_JOBID-$LONG_INDEX.tgz *
	rm -Rf $LOCAL
	if [[ -n $SPID ]]
	then
		rm $HOSTF
	fi
}
