# mysub #

mysub helps you to sumbit set of similar jobs with load balancing.

### When do you need mysub ###

Imagine you have 10k files you'll like to process using cluster running LFS. There are several ways to do this:

* Run a single script processing all the files. This might take a while to process on a single node. There is also high risk of your job getting killed because of timeout.
* Split your files in to let's say 100 sets. Each set will be processed on a single node. This will require some work (splitting files, copying to fast local filesystem, getting the result, etc). Unfortunately nodes and files could be heterogeneous and you can end up in situation when some nodes are free while you have to wait for other nodes to finish.
* Create load balancing system which distributes files to the nodes in fair way. This requires some work and could be painful.

But not anymore.

Just create simple script:


```
#!bash

#!/bin/bash

#BSUB -L /bin/bash
#BSUB -J JOBNAME[1-100]

NAME=JOBNAME
SOURCE=files.tar
source $HOME/mysub/mysub.bash

cmd () {
	some_command -in $1 -out res/$1
}

run
```

Once submitted this will:

* Extract files.tar on local filesystem for every node.
* Each node will run ``some_command`` and request a new job once free .
* All results will be archived transferred to the cluster filesystem in the end.

**Warning**: There are some predefined path variables in mysub. Details on mysub usage will be added later.