#!/usr/bin/env python
import SocketServer
import sys
import thread

class MyTCPHandler(SocketServer.BaseRequestHandler):
    def handle(self):
        global counter, clients
        client = self.request.recv(100).strip()
        clients.add(client)
        d = sys.stdin.readline().strip()
        if d:
            print d
            self.request.sendall(d)
        else:
            self.request.sendall('END')
            counter += 1
            print 'END: %d' % counter
            if counter >= len(clients):
                thread.start_new_thread(self.server.shutdown, ())


if __name__ == "__main__":
    HOST, PORT = "0.0.0.0", int(sys.argv[1])
    counter = 0
    clients = set()
    server = SocketServer.TCPServer((HOST, PORT), MyTCPHandler)
    server.serve_forever()
